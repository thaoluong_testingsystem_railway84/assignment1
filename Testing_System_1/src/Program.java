import java.util.Date;

public class Program {

    public  static  void main (String [] args) {
    //Department

    Department department1 = new Department();
    department1.departmentId = 1;
    department1.departmentName = "Sale";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Marketing";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "Đào Tạo";



        

    // Position
        Position position1 = new Position();
        position1.positionId= 1;
        position1.positionName = PositionName.TEST;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.PM;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.SCRUM_MASTER;






    // Account

        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "T@gmail.com";
        account1.userName = "ThaoLuong123";
        account1.fullName = "Đỗ Lương Thảo";
        account1.departmentId = department1;
        account1.positionId = position3;
        account1.createDate = new Date("1998/11/27");


        Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "T2@gmail.com";
        account2.userName = "ThaoLuong234";
        account2.fullName = "Đỗ Lương Thảo";
        account2.departmentId = department3;
        account2.positionId = position2;
        account2.createDate = new Date("2003/12/01");

        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "T3@gmail.com";
        account3.userName = "ThaoLuong345";
        account3.fullName = "Đỗ Lương Thảo";
        account3.departmentId = department2;
        account3.positionId = position1;
        account3.createDate = new Date("1978/12/01");





        //Group

        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "Nhóm 1";
        group1.creatorId = 1;
        group1.createDate = new Date("2010/08/02");

        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "Nhóm 2";
        group2.creatorId = 2;
        group2.createDate = new Date("2010/02/02");

        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "Nhóm 3";
        group3.creatorId = 3;
        group3.createDate = new Date("2010/03/02");



        //GroupAccount

        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.groupId = group3;
        groupAccount1.accountId = account1;
        groupAccount1.joinDate = new Date("2024/03/15");


        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.groupId = group2;
        groupAccount2.accountId = account3;
        groupAccount2.joinDate = new Date("2024/03/14");


        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.groupId = group1;
        groupAccount3.accountId = account2;
        groupAccount3.joinDate = new Date("2024/03/13");


        // TypeQuestion
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = TypeName.MULTIPLE_CHOICE;

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName = TypeName.ESSAY;

        TypeQuestion typeQuestion3 = new TypeQuestion();
        typeQuestion3.typeId = 3;
        typeQuestion3.typeName = TypeName.MULTIPLE_CHOICE;

        // CategoryQuestion

        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = "JAVA";

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryId = 2;
        categoryQuestion2.categoryName = ".NET";

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryId = 3;
        categoryQuestion3.categoryName = "SQL";

        // Question
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "noi dung cau hoi 1";
        question1.categoryId = categoryQuestion2;
        question1.typeId = typeQuestion3;
        question1.creatorId = 2;
        question1.createDate = new Date("2024/02/11");

        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "noi dung cau hoi 2";
        question2.categoryId = categoryQuestion1;
        question2.typeId = typeQuestion1;
        question2.creatorId = 4;
        question2.createDate = new Date("2024/02/12");

        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "noi dung cau hoi 3";
        question3.categoryId = categoryQuestion3;
        question3.typeId = typeQuestion2;
        question3.creatorId = 2;
        question3.createDate = new Date("2024/02/13");


        //Answer
        Answer answer1 = new Answer();
        answer1.answerId= 1;
        answer1.content="noi dung cau tra loi 1";
        answer1.questionId = question2;
        answer1.isCorrect = true;

        Answer answer2 = new Answer();
        answer2.answerId= 2;
        answer2.content="noi dung cau tra loi 2";
        answer2.questionId = question1;
        answer2.isCorrect = false;

        Answer answer3 = new Answer();
        answer3.answerId= 3;
        answer3.content="noi dung cau tra loi 3";
        answer3.questionId = question3;
        answer3.isCorrect = true;


        // Exam
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "abc123";
        exam1.title = "de thi 1";
        exam1.categoryId = categoryQuestion1;
        exam1.duration = "45 phut";
        exam1.creatorId = question1;
        exam1.createDate = new Date("2024/02/11");

        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = "abc234";
        exam2.title = "de thi 2";
        exam2.categoryId = categoryQuestion2;
        exam2.duration = "45 phut";
        exam2.creatorId = question3;
        exam2.createDate = new Date("2024/02/12");

        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = "abc456";
        exam3.title = "de thi 1";
        exam3.categoryId = categoryQuestion3;
        exam3.duration = "45 phut";
        exam3.creatorId = question2;
        exam3.createDate = new Date("2024/02/13");


        //ExamQuestion
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.examId= exam1.examId;
        examQuestion1.questionId= question1;

        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.examId= exam3.examId;
        examQuestion2.questionId= question3;

        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.examId= exam2.examId;
        examQuestion3.questionId= question2;




        // in ra Department
        System.out.println("//Department");
        System.out.println("departmentId: "+ department2.departmentId);
        System.out.println("departmentName: " + department2.departmentName);

        // in ra Position
        System.out.println("//Position");
        System.out.println("positionId: " + position1.positionName);

        // in ra Account
        System.out.println("//Account");
        System.out.println("accountId: " + account3.accountId );
        System.out.println("Email: " + account3.email);
        System.out.println("userName: " + account3.userName) ;
        System.out.println("fullName: " + account3.fullName) ;
        System.out.println("department: " + account3.departmentId.departmentName) ;
        System.out.println("positionID: " + account3.positionId.positionName) ;
        System.out.println("createDate: " + account3.createDate);


        // in ra Group
        System.out.println("//Group");
        System.out.println("groupId: " + group2.groupId);
        System.out.println("groupName: " + group2.groupName);
        System.out.println("createDate: " + group2.createDate);

        // in ra GroupAccount
        System.out.println("// GroupAccount");
        System.out.println("groupId: " + groupAccount2.groupId);
        System.out.println("AccountId: " + groupAccount2.accountId.fullName);
        System.out.println("JoinDate: " + groupAccount2.joinDate);

        // in ra typeQuestion
        System.out.println("//TypeQuestion");
        System.out.println("typeId: " + typeQuestion1.typeId);
        System.out.println("typeName: " + typeQuestion1.typeName);

        // in ra CategoryQuestion
        System.out.println("// categoryQuestion");
        System.out.println("categoryId: " + categoryQuestion2.categoryId);
        System.out.println("categoryName: " + categoryQuestion2.categoryName);

        // in ra Question
        System.out.println("// Question");
        System.out.println("questionId: " + question3.questionId);
        System.out.println("content: " + question3.content);
        System.out.println("categoryId: " + question3.categoryId.categoryName);
        System.out.println("typeId: " + question3.typeId.typeName);
        System.out.println("creatorId: " + question3.creatorId);
        System.out.println("createDate: " + question3.createDate);

        // in ra Answer
        System.out.println("// Answer");
        System.out.println("answerId: " + answer3.answerId);
        System.out.println("content: " + answer3.content);
        System.out.println("questionId: " + answer3.questionId.questionId);
        System.out.println("isCorrect: " + answer3.isCorrect);


        // in ra Exam
        System.out.println("// Exam");
        System.out.println("examId: " + exam2.examId);
        System.out.println("Code: " + exam2.code);
        System.out.println("title: " + exam2.title);
        System.out.println("categoryId: " + exam2.categoryId.categoryId);
        System.out.println("duration: " + exam2. duration);
        System.out.println("creatorId: " + exam2.creatorId.creatorId);
        System.out.println("createDate: " + exam2.createDate);

        //  in ra ExamQuestion
        System.out.println("//ExamQuestion");
        System.out.println("ExamId: " + examQuestion2.examId);
        System.out.println("QuestionId: " + examQuestion2.questionId.content);



















    }


    }

